#include "crypto.hpp"
#include <iostream>

using namespace std;

int main() {
  cout << "SHA-1 with 1 iteration" << endl;
  cout << Crypto::hex(Crypto::sha1("Test")) << endl
       << endl;

  cout << "SHA-1 with two iterations" << endl;
  cout << Crypto::hex(Crypto::sha1(Crypto::sha1("Test"))) << endl;

  cout << "SHA256 key" << endl;
  cout << Crypto::hex(Crypto::sha256("Test")) << endl;

  cout << "SHA512 key" << endl;
  cout << Crypto::hex(Crypto::sha512("Test")) << endl;

  cout << "The derived key from the PBKDF2 algorithm" << endl;
  cout << Crypto::hex(Crypto::pbkdf2("Password", "Salt")) << endl;

  cout << "MD5 key" << endl;
  cout << Crypto::hex(Crypto::md5("Test")) << endl;

  cout << "Passordet til Ola er ... working: " << endl;
  cout << Crypto::bruteforce("ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6", "Saltet til Ola", 2048) << endl;
}
