#include <iomanip>
#include <iostream>
#include <openssl/evp.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <string>

using namespace std;

// Note: C-style casts, for instance (int), are used to simplify the source code.
//       C++ casts, such as static_cast and reinterpret_cast, should otherwise
//       be used in modern C++.

/// Limited C++ bindings for the OpenSSL Crypto functions.
class Crypto {
public:
  /// Return hex string from bytes in input string.
  static std::string hex(const std::string &input) {
    std::stringstream hex_stream;
    hex_stream << std::hex << std::internal << std::setfill('0');
    for (auto &byte : input)
      hex_stream << std::setw(2) << (int)(unsigned char)byte;
    return hex_stream.str();
  }

  /// Return the MD5 (128-bit) hash from input.
  static std::string md5(const std::string &input) {
    unsigned char digest[16];
    const char *string = input.c_str();

    //printf("string length: %d\n", strlen(string));

    MD5_CTX ctx;
    MD5_Init(&ctx);
    MD5_Update(&ctx, string, strlen(string));
    MD5_Final(digest, &ctx);

    char mdString[33];
    for (int i = 0; i < 16; i++)
      sprintf(&mdString[i * 2], "%02x", (unsigned int)digest[i]);

    printf("md5 digest: %s\n", mdString);
    std::string out(reinterpret_cast<char *>(digest));
    return out;

    // throw std::logic_error("not yet implemented");
  }

  /// Return the SHA-1 (160-bit) hash from input.
  static std::string sha1(const std::string &input) {
    std::string hash;
    hash.resize(160 / 8);
    SHA1((const unsigned char *)input.data(), input.size(), (unsigned char *)hash.data());
    return hash;
  }

  /// Return the SHA-256 (256-bit) hash from input.
  static std::string sha256(const std::string &input) {
    unsigned char digest[SHA256_DIGEST_LENGTH];
    const char *string = input.c_str();

    SHA256_CTX ctx;
    SHA256_Init(&ctx);
    SHA256_Update(&ctx, string, strlen(string));
    SHA256_Final(digest, &ctx);

    char mdString[SHA256_DIGEST_LENGTH * 2 + 1];
    for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
      sprintf(&mdString[i * 2], "%02x", (unsigned int)digest[i]);

    // printf("SHA256 digest: %s\n", mdString);
    std::string out(reinterpret_cast<char *>(digest));
    return out;
    //throw std::logic_error("not yet implemented");
  }

  /// Return the SHA-512 (512-bit) hash from input.
  static std::string sha512(const std::string &input) {
    unsigned char digest[SHA512_DIGEST_LENGTH];
    const char *string = input.c_str();

    SHA512_CTX ctx;
    SHA512_Init(&ctx);
    SHA512_Update(&ctx, string, strlen(string));
    SHA512_Final(digest, &ctx);

    char mdString[SHA512_DIGEST_LENGTH * 2 + 1];
    //   for (int i = 0; i < SHA512_DIGEST_LENGTH; i++)
    //     sprintf(&mdString[i * 2], "%02x", (unsigned int)digest[i]);

    //printf("SHA512 digest: %s\n", mdString);
    std::string out(reinterpret_cast<char *>(digest));
    return out;

    // throw std::logic_error("not yet implemented");
  }

  /// Return key from the Password-Based Key Derivation Function 2 (PBKDF2).
  static std::string pbkdf2(const std::string &password, const std::string &salt, int iterations = 4096, int key_length_in_bits = 256) {
    auto key_length_in_bytes = key_length_in_bits / 8;
    std::string key;
    key.resize(key_length_in_bytes);
    auto success = PKCS5_PBKDF2_HMAC_SHA1(password.data(), password.size(),
                                          (const unsigned char *)salt.data(), salt.size(), iterations,
                                          key_length_in_bytes, (unsigned char *)key.data());
    if (!success)
      throw std::runtime_error("openssl: error calling PBKCS5_PBKDF2_HMAC_SHA1");
    return key;
  }
  static std::string bruteforce(const std::string &hash, const std::string &salt, int iterations) {
    std::string output = hash;
    std::string password = "";
    bool hackerman = false;
    // string alphabet[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    string alphabet[] = {"Q", "E", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
    //string alphabet[] = {"Q", "q", "W", "w", "E", "e"};

    int l = 28;
    //cout << hash << endl;
    //output = Crypto::hex(hash);

    //cout << "Output = " + output << endl;
    //cout << "QWEPBKDF = " + Crypto::hex(pbkdf2("Qwe", "Saltet til Ola", 2048, 160)) << endl;

    int hacker = 0;

    for (int i = 0; i < l; i++) {
      for (int j = 0; j < l; j++) {
        for (int k = 0; k < l; k++) {
          password = alphabet[i] + alphabet[j] + alphabet[k];
          ++hacker;
          // cout << hacker << endl;
          if (Crypto::hex(pbkdf2(password, salt, iterations, 160)) == output) {
            hackerman = true;
            return password;
          } else {
            continue;
          }
        }
        password = "";
      }
      password = "";
    }

    return "Feilet";
  }
};
